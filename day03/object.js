/*
 * @Description: 对象
 * @Author: rendc
 * @Date: 2022-06-08 15:15:52
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-08 15:30:06
 */

let dest, src, result;

dest = { id: 'dest' };
src = { id: 'src' };

result = Object.assign(dest, src)
// console.log('🤡 CC - src', src)
// console.log('🤡 CC - dest', dest)
// console.log('🤡 CC - result', result)


let name = "张三"
let age = 23
let gender = "男"

let per = {
  name,
  age,
  gender
}
// console.log('🤡 CC - per', per)

// const nameKey = "name";
// let person = {};
// person.[nameKey] = "张三"
// console.log('🤡 CC - person', person)

const nameKey = "name"
let person = {
  [nameKey]: '张三'
}
console.log('🤡 CC - person', person)
