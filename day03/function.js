/*
 * @Description: 函数
 * @Author: rendc
 * @Date: 2022-06-08 15:40:44
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-08 15:45:11
 */

function sayHello (name) {
  console.log(`hello,${name}`);
}

console.log(sayHello("张三"));

function add (a, b) {
  return a + b
}

console.log(add(2, 3));