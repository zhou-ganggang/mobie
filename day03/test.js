/*
 * @Description: 测试
 * @Author: rendc
 * @Date: 2022-06-08 10:18:07
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-08 15:03:43
 */
// "use strict";

// console.log(parseInt(070));

console.log(parseInt("70", 8));
console.log(parseInt("70", 10));


var str = "string"
var str = 'string'
var str = `string`
console.log('🤡 CC - str', str)

console.log("a\nb");
console.log("a\\b");


var str = "hello,"
str = str + "world" // "hello,world"
console.log('🤡 CC - str', str)

var num = 0
console.log('🤡 CC - num', num)
console.log(num.toString());

var a = true
console.log(typeof a);
console.log(typeof a.toString());

var num = 10;
num.toString()
console.log(num.toString());
console.log(num.toString(2));
console.log(num.toString(8));
console.log(num.toString(10));
console.log(num.toString(16));

var str = num + ""
console.log(str);
console.log(typeof str);

console.log(String(null));
console.log(String(undefined));


var str = "a\nb"
console.log(str)


var str = `a
b`
console.log(str)


var html = `
<div>div</div>
`
let value = 5;
let string = "第" + value + "次"
console.log('🤡 CC - string', string)
let newString = `第${value}次`
console.log('🤡 CC - newString', newString)

let symbol = Symbol();
console.log(typeof symbol);

let obj = new Object();
// 添加属性
obj.name = "zhangsan";
obj.age = 23;
obj.gender = "男"
// 添加方法
obj.sayName = function () {
  console.log(`我的名字叫${this.name}，我的年龄是${this.age}，我的性别是${this.gender}`);
}
let person = {
  // 属性
  name: "lisi",
  age: 24,
  gender: "男",
  // 方法
  sayName () {
    console.log(`我的名字叫${this.name}，我的年龄是${this.age}，我的性别是${this.gender}`);
  }
}
let person1 = {}
console.log('🤡 CC - person1', person1)
// 给person创建一个只读的name属性
Object.defineProperty(person1, "name", {
  writable: false,
  value: 'lisi'
})
// 在严格模式下修改只读的值会报错 非严格模式下不会报错 但是修改的操作会被忽略
person1.name = "zhangxsan"
console.log('🤡 CC - person1', person1.name)

// console.log(typeof obj);
// console.log(typeof person);

// console.log(obj.name);
// person.sayName()
// obj.sayName()



let person2 = {}
// 给person2创建一个不可删除的name属性
Object.defineProperty(person2, "name", {
  writable: true,
  configurable: false,
  value: 'zhangsan'
})
// 在严格模式下修改只读的值会报错 非严格模式下不会报错 但是修改的操作会被忽略
person2.name = "lis"

delete person2.name
console.log('🤡 CC - person2', person2.name)