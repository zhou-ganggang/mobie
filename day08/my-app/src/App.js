/*
 * @Description: App
 * @Author: rendc
 * @Date: 2022-06-15 10:25:31
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-15 14:46:24
 */
import './App.css';
import Main from './page/Main'
import PersonalCenter from './page/PersonalCenter'
import Todo from './page/Todo'
import Message from './page/Message'
import React from 'react';
import { TabBar } from 'antd-mobile'
import {
  AppOutline,
  MessageOutline,
  UnorderedListOutline,
  UserOutline,
} from 'antd-mobile-icons'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 'Main'
    }
    this.setActiveKey = this.setActiveKey.bind(this);
  }
  setActiveKey (props) {
    this.setState(state => ({
      page: props
    }));

  }
  render () {
    const tabs = [
      {
        key: 'Main',
        title: '首页',
        icon: <AppOutline />,
      },
      {
        key: 'todo',
        title: '我的待办',
        icon: <UnorderedListOutline />,
      },
      {
        key: 'message',
        title: '我的消息',
        icon: <MessageOutline />,
      },
      {
        key: 'personalCenter',
        title: '个人中心',
        icon: <UserOutline />,
      },
    ]
    let MyDiv
    let page = this.state.page
    if (page === "Main") {
      MyDiv = <Main />
    } else if (page === "personalCenter") {
      MyDiv = <PersonalCenter />
    } else if (page === "todo") {
      MyDiv = <Todo />
    } else if (page === "message") {
      MyDiv = <Message />
    }
    return (
      <div className="App">
        <div style={{ height: "calc(100vh - 50px)", }}>
          {MyDiv}
        </div>
        <TabBar
          onChange={this.setActiveKey}
        >
          {tabs.map(item => (
            <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
          ))}
        </TabBar>
      </div>
    );
  }
}

export default App;
