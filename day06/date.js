/*
 * @Description: Data
 * @Author: rendc
 * @Date: 2022-06-13 10:21:35
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-13 10:54:42
 */
let date = new Date()
console.log('🤡 CC - date', date)

console.log(date.getFullYear());// 是哪一年
console.log(date.getMonth() + 1);// 是几月？ 0-11
console.log(date.getDate());// 是几号？
console.log(date.getDay());// 是周几？
console.log(date.getHours());// 几点了？
console.log(date.getMinutes());// 几分了？
console.log(date.getSeconds());// 几秒了？

console.log(date.getTime());// 几秒了？
// 使用new Date() 在控制台输出以下格式的时间
// 现在是2022年6月13日 星期1 10:29:30


let dateStr = "2022-06-12"

// let date1 = new Date(dateStr)
let date1 = new Date()

let year = date1.getFullYear();
let month = date1.getMonth() + 1;
let day = date1.getDate();
let week = date1.getDay() === 0 ? '日' : date1.getDay()// 0-6 周日是0
let hour = date1.getHours()
let minute = date1.getMinutes()
let second = date1.getSeconds()

console.log(`现在是${year}年${month}月${day}日 星期${week} ${hour}:${minute}:${second}`);

// 2022-06-13 00:00:00
console.log(`${year}-${month}-${day} ${hour}:${minute}:${second}`);


// 2022-06-13 
console.log(`${year}-${month}-${day}`);


// 2022|06|13 
console.log(`${year}|${month}|${day}`);




var moment = require('moment')

var m = moment()
console.log(m.format("YYYY-MM-DD"));
console.log(m.format("YYYY~MM~DD"));
console.log(m.format("YYYY|MM|DD HH:mm:ss"));
