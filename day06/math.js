/*
 * @Description: Math对象
 * @Author: rendc
 * @Date: 2022-06-13 14:35:12
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-13 14:48:41
 */

// console.log(Math.E);
// console.log(Math.LN10);
// console.log(Math.LN2);
// console.log(Math.PI);

// console.log(Math.min(1, 3, 5, 4, 122, 29, 34));
// console.log(Math.max(1, 3, 5, 4, 122, 29, 34));

// console.log(Math.ceil(25.9));
// console.log(Math.ceil(25.1));

// console.log(Math.floor(25.9));
// console.log(Math.floor(25.1));

// console.log(Math.round(25.9));
// console.log(Math.round(25.1));

// console.log(Math.fround(25.9));
// console.log(Math.fround(25.1));

// console.log(Math.random());
// 1-10的随机数
// console.log(Math.floor(Math.random() * 10 + 1));