/*
 * @Description: 正则表达式
 * @Author: rendc
 * @Date: 2022-06-13 11:07:17
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-13 11:36:49
 */
// let pattern = /abc/ig
// 匹配字符串中所有的abc

// abccABCAbc1aBC

// let str = "ef ab 12 3abc abcd 33"
// let pattern = /ab/ig
// console.log(pattern.exec(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.exec(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.exec(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.exec(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)


// let str = "ef ab 12 3abc abcd 33"
// let pattern = /ab/i
// console.log(pattern.test(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.test(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.test(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)
// console.log(pattern.test(str));
// console.log('🤡 CC - pattern', pattern.lastIndex)

// 查找出以三个字母开头的一句话
let pattern = /^[A-Z]{3}\w*/ig
let s1 = 'www123'
// console.log(pattern.test(s1));
let s2 = '1www23'
// console.log(pattern.test(s2));


// 查找出以三个字母开头,并且以三个小写字母结尾的一句话
let pattern1 = /^[A-Za-z]{3}\w*[a-z]{3}$/g
// console.log(pattern1.test(s2));
let s3 = 'wwW1234123fwerfwer'
// console.log(pattern1.test(s3));

// 匹配2-5个数字
let pattern2 = /\d{2,3}/img
let s4 = "1234"
// console.log(pattern2.test(s4));

// 匹配三个字母和一个可选数字
let pattern3 = /[a-z]{3}\d?/img
let s5 = "aaa1234"
console.log(pattern3.test(s5));