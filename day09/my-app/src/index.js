/*
 * @Description: 
 * @Author: rendc
 * @Date: 2022-06-15 10:25:31
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-15 15:14:39
 */
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Login from './page/Login';
import reportWebVitals from './reportWebVitals';
import { Toast } from 'antd-mobile';
let isLogin = window.sessionStorage.getItem('user')

let show = isLogin ? <App /> : <Login />
if (!isLogin) {
  Toast.show({
    icon: 'fail',
    content: '用户信息已过期，请重新登录',
  })
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {show}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
