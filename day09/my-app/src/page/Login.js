/*
 * @Description: 登录
 * @Author: rendc
 * @Date: 2022-06-15 14:48:04
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-15 15:13:15
 */


import React from 'react';
import { Form, Input, PasscodeInput, Button, Toast } from 'antd-mobile'

class PersonalCenter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render () {
    const onFinish = (values) => {
      let { name, password } = values
      Toast.show({
        icon: 'loading',
        content: '登录中...',
        duration: 2000,
      })
      setTimeout(() => {
        if (name === "admin" && password === "123321") {
          // 登录成功
          Toast.show({
            icon: 'success',
            content: '登录成功',
          })
          // 保存用户信息到session
          window.sessionStorage.setItem('user', name)
          window.location.reload()
        } else {
          // 登录失败
          Toast.show({
            icon: 'fail',
            content: '用户名或密码错误',
          })
        }
      }, 2000);
    }
    return (
      <div>
        <div>
          登录页面
        </div>
        <Form layout='horizontal' mode='card' onFinish={onFinish}>
          <Form.Item label='用户名' name="name">
            <Input placeholder='请输入用户名' />
          </Form.Item>
          <Form.Item label='密码' name="password">
            <PasscodeInput />
          </Form.Item>
          <Form.Item>
            <Button block type='submit' color='primary' size='large'>
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    )
  }
}

export default PersonalCenter