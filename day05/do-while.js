/*
 * @Description: do-while
 * @Author: rendc
 * @Date: 2022-06-10 11:29:22
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-10 11:30:47
 */
do {
  console.log('循环体里的代码先执行，然后才判断条件是否满足condition');
} while (condition);
// 然后才判断条件是否满足condition