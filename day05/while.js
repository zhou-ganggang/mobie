/*
 * @Description: while
 * @Author: rendc
 * @Date: 2022-06-10 11:29:22
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-10 11:32:49
 */
while (condition) {
  // 先判断是否满足condition
  // 条件满足的情况才会执行循环体里的代码
}