/*
 * @Description: 操作符
 * @Author: rendc
 * @Date: 2022-06-10 11:00:25
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-10 11:24:26
 */

// let age = 29;

// ++age
// // let age = 29;
// // age = age + 1;
// // 30

// age++ 

// let age = 29;
// let age2 = --age + 2;
// console.log('🤡 CC - age', age)   // 28
// console.log('🤡 CC - age2', age2) // 30


let age = 29;
let age2 = age-- + 2;
console.log('🤡 CC - age', age)   // 28
console.log('🤡 CC - age2', age2) // 31?后缀版的递增和递减在语句被求值之后才发生

function max (a, b) {
  console.log(a > b ? a : b);
}
max(5, 10)