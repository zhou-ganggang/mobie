/*
 * @Description: if语句
 * @Author: rendc
 * @Date: 2022-06-10 11:26:27
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-10 11:28:45
 */
if (condition) {
  // condition满足时执行的代码
}


if (condition) {
  // condition满足时执行的代码

} else {
  // condition不满足时执行的代码

}

if (condition) {
  // condition满足时执行的代码

} else if (condition2) {
  // condition2满足时执行的代码

} else {
  // condition1、2都不满足时执行的代码

}