/*
 * @Description: 数组
 * @Author: rendc
 * @Date: 2022-06-10 08:55:20
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-10 10:33:13
 */
function getArray () {
  console.log('🤡 CC - getArray - arguments', arguments)
  console.log('🤡 CC - getArray - Array.from(arguments)', Array.from(arguments))
}

getArray(1, 2, 3, 4, 5)

console.log(Array.from("Hello")); //[ 'H', 'e', 'l', 'l', 'o' ]

let a1 = [1, 2, 3]
let a2 = Array.from(a1)
let a3 = Array.from(a1, x => x * 2)
console.log('🤡 CC - a3', a3)

console.log('🤡 CC - a2', a2)
console.log(a1 === a2);


console.log(Array.of(1, 2, 3, 4));// [ 1, 2, 3, 4 ]

let arr = [, , , , ,]
console.log('🤡 CC - arr', arr)
let names = ["张三", "李四", "王五"]
console.log(names[0]);
names[0] = "法外狂徒" // 修改第一项
names[4] = "金针菇"  // 添加第五项
// console.log(names[0]);
// console.log(names);
// console.log(Array.isArray(names));
console.log('🤡 CC - names', names)
console.log(Array.from(names.keys()));
console.log(Array.from(names.values()));
console.log(Array.from(names.entries()));


let arr1 = [0, 0, 0, 0, 0]
console.log('🤡 CC - arr1', arr1)
arr1.fill(5) // 全部用5填充
console.log('🤡 CC - arr1', arr1)
arr1.fill(0) // 全部用0填充
console.log('🤡 CC - arr1', arr1)
arr1.fill(5, 3) // 用5填充索引大于等于3的元素
console.log('🤡 CC - arr1', arr1)
arr1.fill(0) // 全部用0填充
console.log('🤡 CC - arr1', arr1)
arr1.fill(5, 1, 3) // 用5填充索引大于等于1小于3的元素,
console.log('🤡 CC - arr1', arr1)

arr1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
console.log('🤡 CC - arr1', arr1)
arr1.copyWithin(5) // 复制索引从0开始的内容，插入到索引为5的位置
console.log('🤡 CC - arr1', arr1)

arr1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
console.log('🤡 CC - arr1', arr1)
arr1.copyWithin(0, 5) // 复制索引从5开始的内容，插入到索引为0的位置
console.log('🤡 CC - arr1', arr1)

arr1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
console.log('🤡 CC - arr1', arr1)
arr1.copyWithin(4, 0, 3) // 复制索引从0开始到3结束的内容，插入到索引为4的位置
console.log('🤡 CC - arr1', arr1)

arr1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// console.log('🤡 CC - arr1', arr1)
// console.log('🤡 CC - arr1', arr1.toString())
console.log('🤡 CC - arr1', arr1)
console.log('🤡 CC - arr1--', arr1.valueOf())
// console.log('🤡 CC - arr1', arr1)
let arr2 = ["张三", "李四", "王五"]
console.log('🤡 CC - arr2', arr2.toLocaleString())
console.log('🤡 CC - arr2', arr2.toString())
console.log('🤡 CC - arr2', arr2)
console.log('🤡 CC - arr2', arr2.valueOf())

console.log(arr2.push("赵六"));
console.log('🤡 CC - arr2', arr2)

console.log(arr2.pop());
console.log('🤡 CC - arr2', arr2)

console.log(arr2.shift());
console.log('🤡 CC - arr2', arr2)

console.log(arr2.unshift('张三', '张三'));
console.log('🤡 CC - arr2', arr2)
console.log('🤡 CC - arr2', arr2.reverse())


let arr3 = [1, 2, 5, 7, 4, 6, 3]
console.log(arr3.sort());

console.log(arr3.concat('999', [1, 2]));

let arr4 = [1, 2, 3, 4, 5, 6, 7]
// console.log('+++', arr4.slice(1));
console.log('+++', arr4.slice(0, 1));// (]

let arr5 = [1, 2, 3, 4, 5, 6, 7]
// console.log(arr5.splice(0, 2));
// console.log(arr5.splice(0, 0, 999, 999));
console.log(arr5.splice(0, 2, 777, 777));
console.log(arr5);