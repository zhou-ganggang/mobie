/*
 * @Description: function.js
 * @Author: rendc
 * @Date: 2022-06-09 09:14:49
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-09 11:00:48
 */
let add = a => {
  return a
}


let myAdd = (a, b) => {
  return a + b
}
let arr = [5, 6]
// console.log('---' + myAdd(...arr));

function getSum (...values) {
  return values.reduce((x, y) => x + y)
}

// console.log(getSum(1, 2, 3));

let myAdd1 = (a, b) => a + b

// console.log(myAdd1(3, 5));

let myAdd2 = myAdd1
// console.log('🤡 CC - myAdd1', myAdd1)
// console.log('🤡 CC - myAdd2', myAdd2)

// console.log(myAdd2());

// sum(2, 3)
// function sum (a, b) {
//   console.log(a + b);
// }


// sum(2, 3) //会报错
// let sum = function (a, b) {
//   console.log(a + b);
// }