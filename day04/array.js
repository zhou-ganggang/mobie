/*
 * @Description: 数组
 * @Author: rendc
 * @Date: 2022-06-09 11:05:06
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-09 11:08:30
 */

let arr = [1, 2, 3]
console.log('🤡 CC - arr', arr)
console.log('🤡 CC - arr', arr.length)
arr.length = 1
console.log('🤡 CC - arr', arr)
console.log('🤡 CC - arr', arr.length)
arr.length = 3
console.log('🤡 CC - arr', arr)
console.log('🤡 CC - arr', arr.length)
let arr1 = ["1", "2", "3"]
let arr3 = [1, "2", true, [1, 2], arr]
// console.log('🤡 CC - arr3', arr3)

