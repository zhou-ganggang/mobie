/*
 * @Description: arguments
 * @Author: rendc
 * @Date: 2022-06-09 09:33:36
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-09 10:54:24
 */

function myAdd () {
  // console.log('🤡 CC - myAdd - a,b', a, b)
  console.log('🤡 CC - myAdd - arguments', arguments)
  console.log(arguments.callee);
  console.log(arguments[0]);
  console.log(arguments[1]);
  console.log(`有${arguments.length}个参数`);
}
// 类数组对象
// [Arguments] { '0': 2, '1': 3 }
myAdd(2, 3)