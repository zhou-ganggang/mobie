/*
 * @Description: 分类
 * @Author: rendc
 * @Date: 2022-06-15 10:29:19
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-16 15:59:13
 */

import React from 'react';
import { SideBar } from 'antd-mobile'
import Key1 from './todo/Key1'
import Key2 from './todo/Key2'



class Todo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeKey: "key1",
    }
    this.setActiveKey = this.setActiveKey.bind(this);
  }
  setActiveKey = (key) => {
    this.setState(state => ({
      activeKey: key
    }));
  }
  render () {
    const tabs = [
      {
        key: 'key1',
        title: '有品推荐',
      },
      {
        key: 'key2',
        title: '手机数码',
      },
      {
        key: 'key3',
        title: '小米电视',
      },
      {
        key: 'key4',
        title: '影音娱乐',
      },
      {
        key: 'key5',
        title: '大家电',
      },
      {
        key: 'key6',
        title: '智能家庭',
      },
      {
        key: 'key7',
        title: '厨卫电器',
      },
      {
        key: 'key8',
        title: '生活家电',
      },
      {
        key: 'key9',
        title: '美食酒饮',
      },
      {
        key: 'key10',
        title: '家具家装',
      },
      {
        key: 'key11',
        title: '电脑办公',
      },
    ]
    let MyDiv
    let activeKey = this.state.activeKey
    // 此处的if else后面需要优化
    if (activeKey === "key1") {
      MyDiv = <Key1 />
    } else if (activeKey === "key2") {
      MyDiv = <Key2 />
    }
    // else if (activeKey === "Key3") {
    //   MyDiv = <Key3 />
    // } else if (activeKey === "message") {
    //   MyDiv = <Message />
    // }
    return (
      <div
        id='app'
      >
        <div className='container'>
          <div className='side'>
            <SideBar
              onChange={this.setActiveKey}>
              {tabs.map(item => (
                <SideBar.Item key={item.key} title={item.title} />
              ))}
            </SideBar>
          </div>
          <div className='content' >
            {MyDiv}
          </div>
        </div>
      </div>
    )
  }
}

export default Todo