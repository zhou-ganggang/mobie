/*
 * @Description: 商品详情页
 * @Author: rendc
 * @Date: 2022-06-17 09:09:02
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-17 11:13:16
 */

import React from 'react';
import { Swiper, Toast, Image, Button, NavBar, Space } from 'antd-mobile'
import { SearchOutline, MoreOutline, HandPayCircleOutline } from 'antd-mobile-icons'
class Details extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render () {
    const right = (
      <div style={{ fontSize: 24 }}>
        <Space style={{ '--gap': '16px' }}>
          <SearchOutline />
          <MoreOutline />
        </Space>
      </div>
    )
    const collectImg = "//cdn.cnbj1.fds.api.mi-img.com/mijia-m/production/yrn-buz-shop-center/res/images/product/icon_product_unfavor.png"
    const images = [
      'https://img.youpin.mi-img.com/shopmain/6fd3a0ea0a87171951a7ce68942cca66.jpg?w=1080&h=1080',
      'https://img.youpin.mi-img.com/shopmain/a0ccec943ec80b95c0a9bf660442cc55.jpg?w=1080&h=1080',
      'https://img.youpin.mi-img.com/shopmain/3c3dc9f82d9bcaf1d9479125cf184594.jpg?w=1080&h=1080',
      'https://img.youpin.mi-img.com/shopmain/49cb7725481ffe3a27eb3c0f3f7db31c.jpg?w=1080&h=1080',
      'https://img.youpin.mi-img.com/shopmain/199db904cbb3dea55a9f19157e1868a8.jpg?w=1080&h=1080'
    ]
    const items = images.map((img, index) => (
      <Swiper.Item key={index}>
        <div
          onClick={() => {
            Toast.show(`你点击了卡片 ${index + 1}`)
          }}
        >
          <Image
            src={img}
            fit='cover'
            style={{ borderRadius: 8 }}
          />
        </div>
      </Swiper.Item>
    ))
    return (
      <div >
        <NavBar right={right} onBack={() => {
          window.history.back()
        }}>

        </NavBar>
        <div className='top'>
          <Swiper
            loop
            autoplay
            style={{ '--height': '375px' }}
            indicator={(total, current) => (
              <div className="customIndicator">
                {`${current + 1} / ${total}`}
              </div>
            )}
          >{items}
          </Swiper>
          <div>
            <div className='priceTop'>
              <div className='price' >
                <div className='price1'>￥</div>
                <div className='price2'>179</div>
              </div>
              <div className='collect' >
                <Image
                  src={collectImg}
                  fit='cover'
                  width={40}
                  style={{ borderRadius: 8 }}
                />
              </div>
            </div>
            <br /><br /><br /><br /><br />
            <div>
              【巨省电】 | 小米新1级空调（1.5匹/变频/新一级能效）<br /><br />
              【巨省电】 | 小米新1级空调（1.5匹/变频/新一级能效）<br /><br />
              【极速制冷】性能强劲，凉爽快速覆盖全屋<br /><br />
              【极速制冷】性能强劲，凉爽快速覆盖全屋<br /><br />
              【高效节能】新一级能效，比老一级能效省电91度<br /><br />
              【高效节能】新一级能效，比老一级能效省电91度<br /><br />
              【智能互联】远程操控，接入米家全屋智能生态<br /><br />
              【智能互联】远程操控，接入米家全屋智能生态<br /><br />
            </div>
          </div>
        </div>
        <div className='bottom'>
          <HandPayCircleOutline fontSize={40} style={{ float: "left" }} onClick={() => {
            window.location.href = "/ShoppingCar"
          }} />
          <Button size='middle' shape='rounded' color='primary' onClick={() => {

            let oldShopping = window.sessionStorage.getItem('shoppingCar')
            console.log('🤡 CC - Details - render - oldShopping', oldShopping)
            if (oldShopping === null) {
              let shopping = [{ name: "空调", price: 179, time: new Date().toLocaleTimeString() },]
              window.sessionStorage.setItem('shoppingCar', JSON.stringify(shopping))
            } else {
              let shopping = { name: "空调", price: 179, time: new Date().toLocaleTimeString() }
              let oldShoppings = JSON.parse(oldShopping)
              oldShoppings.push(shopping)
              window.sessionStorage.setItem('shoppingCar', JSON.stringify(oldShoppings))
            }
          }}>
            加入购物车
          </Button>
          <Button size='middle' shape='rounded' color='primary'>
            立即购买
          </Button>
        </div>
      </div >
    )
  }
}

export default Details