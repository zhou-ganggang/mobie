/*
 * @Description: 购物车
 * @Author: rendc
 * @Date: 2022-06-17 10:46:01
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-17 11:18:07
 */

import React from 'react';
import { NavBar, Space } from 'antd-mobile'
import { SearchOutline, MoreOutline, } from 'antd-mobile-icons'
class ShoppingCar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render () {
    const right = (
      <div style={{ fontSize: 24 }}>
        <Space style={{ '--gap': '16px' }}>
          <SearchOutline />
          <MoreOutline />
        </Space>
      </div>
    )
    const shoppingCar = JSON.parse(window.sessionStorage.getItem("shoppingCar"))
    const Shopping = shoppingCar.map((item, index) => {
      return (
        <div key={index}>{index}.{item.time}:{item.name}:{item.price}</div>
      )
    })
    return (
      <div >
        <NavBar right={right} onBack={() => {
          window.history.back()
        }}>
        </NavBar>
        购物车
        {Shopping}
      </div >
    )
  }
}

export default ShoppingCar