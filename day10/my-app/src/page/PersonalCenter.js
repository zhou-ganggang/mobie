/*
 * @Description: 个人中心
 * @Author: rendc
 * @Date: 2022-06-15 10:29:19
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-16 15:25:16
 */

import { Image, Button, Dialog } from 'antd-mobile';
import React from 'react';

class PersonalCenter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render () {
    const demoSrc =
      'https://images.unsplash.com/photo-1620476214170-1d8080f65cdb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3150&q=80'
    const name = window.sessionStorage.getItem('user')
    const quitHandler = () => {
      Dialog.confirm({
        content: '确定要退出当前账号吗？',
        closeOnAction: true,
        // actions: [
        //   [
        //     {
        //       key: 'cancel',
        //       text: '取消',
        //     },
        //     {
        //       key: 'delete',
        //       text: '退出',
        //       bold: true,
        //       danger: true,
        //     },
        //   ],
        // ],
        onConfirm: () => {
          window.sessionStorage.removeItem('user')
          window.location.reload()
        },
      })

    }
    return (
      <div
        id='app'>
        <Image
          src={demoSrc}
          width={128}
          height={128}
          fit='cover'
          style={{ borderRadius: "50%", margin: "0px auto", }}
        />
        <div
          style={{ color: "#777", fontSize: "26px", margin: "30px", textAlign: "center" }}
        >{name}</div>
        <div
          style={{ margin: "20px 0" }}>
          <Button block size='large'>
            设置
          </Button>
        </div>
        <div
          style={{ margin: "20px 0" }}>
          <Button block size='large'>
            青少年模式
          </Button>
          <Button block size='large'>
            老年人模式
          </Button>
          <Button block size='large'>
            关于
          </Button>
        </div>
        <div
          style={{ margin: "20px 0" }}>
          <Button block size='large'>
            青少年模式
          </Button>
          <Button block size='large'>
            老年人模式
          </Button>
          <Button block size='large'>
            关于
          </Button>
        </div>
        <div
          style={{ margin: "20px 0" }}>
          <Button block size='large'>
            个人信息与权限
          </Button>
          <Button block size='large'>
            自定义推送设置
          </Button>
        </div>
        <Button color='danger' block size='large' onClick={quitHandler}>
          退出登录
        </Button>
      </div>
    )
  }
}

export default PersonalCenter