/*
 * @Description: key1
 * @Author: rendc
 * @Date: 2022-06-16 09:29:38
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-17 10:41:15
 */

import React from 'react';
import { Swiper, Toast, Image } from 'antd-mobile'
import { createBrowserHistory } from 'history'


class Key1 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render () {
    const history = createBrowserHistory()
    const colors = ['https://img.youpin.mi-img.com/ferriswheel/6429629d_cd2e_435c_92e4_bbb32f9097c0.png?w=786&h=228', 'https://img.youpin.mi-img.com/ferriswheel/e6a71699_2b7e_451d_9114_73208234cdd8.jpeg?w=786&h=228', 'https://img.youpin.mi-img.com/ferriswheel/8e8a526e_002b_4538_add6_4d312ae4eda5.jpeg?w=786&h=228', 'https://img.youpin.mi-img.com/ferriswheel/65388d75_4cf9_4d18_88f0_016f8d7a90e5.jpeg?w=786&h=228']
    const items = colors.map((img, index) => (
      <Swiper.Item key={index}>
        <div
          className="mySwiper"
          onClick={() => {
            Toast.show(`你点击了卡片 ${index + 1}`)
          }}
        >
          <Image
            src={img}
            fit='cover'
            style={{ borderRadius: 8 }}
          />
        </div>
      </Swiper.Item>
    ))
    const shoppingList = [
      { img: "https://img.youpin.mi-img.com/ink/dc4892d4_81f2_4d16_ae9f_fd1d31ebff9f.png?w=228&h=228", shoppingName: "小米空调" },
      { img: "https://img.youpin.mi-img.com/ink/e086b69c_908e_489e_9396_c8c5db0d1f66.png?w=228&h=228", shoppingName: "小米冰箱" },
      { img: "https://img.youpin.mi-img.com/ink/7c1ce642_16ee_43a2_a95d_c33568dda752.png?w=228&h=228", shoppingName: "小米洗衣机" },
      { img: "https://img.youpin.mi-img.com/ink/75652be9_196b_4236_a5d7_5307981c9e61.png?w=228&h=228", shoppingName: "小米大家电" },
      { img: "https://img.youpin.mi-img.com/ink/e4381171_1873_4a7d_9877_5ed1ba8873c0.png?w=228&h=228", shoppingName: "云米精选" },
      { img: "https://img.youpin.mi-img.com/ink/e045e16e_0feb_4a86_8cad_45b04f96a15d.png?w=228&h=228", shoppingName: "颜值电器" },
    ]
    const shoppingList1 = [
      { img: "https://img.youpin.mi-img.com/ink/7c1ce642_16ee_43a2_a95d_c33568dda752.png?w=228&h=228", shoppingName: "小米洗衣机" },
      { img: "https://img.youpin.mi-img.com/ink/75652be9_196b_4236_a5d7_5307981c9e61.png?w=228&h=228", shoppingName: "小米大家电" },
      { img: "https://img.youpin.mi-img.com/ink/e4381171_1873_4a7d_9877_5ed1ba8873c0.png?w=228&h=228", shoppingName: "云米精选" },
      { img: "https://img.youpin.mi-img.com/ink/e045e16e_0feb_4a86_8cad_45b04f96a15d.png?w=228&h=228", shoppingName: "颜值电器" },
    ]
    // const shoppingDiv = shoppingList.map((array, index) => (
    //   (
    //     <div className='shopping' key={'shopping' + index}>
    //       <div>
    //         <Image
    //           src={array.img}
    //           width={64}
    //           height={64}
    //           fit='cover'
    //           style={{ borderRadius: 8 }}
    //         />
    //       </div>
    //       <div className='name'>{array.shoppingName}</div>
    //     </div>
    //   )
    // ))
    const shoppingDivs = (shoppingList) => {
      return shoppingList.map((array, index) => (
        (<div className='shopping' key={'shopping' + index} onClick={() => {
          history.push('/Details')
          window.location.reload()
        }}>
          <div>
            <Image
              src={array.img}
              width={64}
              height={64}
              fit='cover'
              style={{ borderRadius: 8 }}
            />
          </div>
          <div className='name'>{array.shoppingName}</div>
        </div>)
      ))
    }
    const cardList = [
      { title: "精选分类", shoppingList },
      { title: "精选专区", shoppingList: shoppingList1 },
      { title: "精选专区", shoppingList: shoppingList1 },
    ]
    const CardDiv = cardList.map((array, index) => (
      (
        <div className='myCard' key={"cardDiv" + index}>
          <div className='title'>{array.title}</div>
          {shoppingDivs(array.shoppingList)}
        </div>
      )
    ))
    return (
      <div>
        <div>
          <Swiper loop autoplay>{items}</Swiper>
        </div>
        {/* <div className='myCard'>
          <div className='title'>热门精选</div>
          {shoppingDiv}
        </div> */}
        {CardDiv}
      </div >

    )
  }
}

export default Key1