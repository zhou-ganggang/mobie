/*
 * @Description: like_button
 * @Author: rendc
 * @Date: 2022-06-14 14:45:51
 * @LastEditors: rendc
 * @LastEditTime: 2022-06-14 14:52:08
 */
'use strict';

const e = React.createElement;

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render () {
    if (this.state.liked) {
      return 'You liked this.';
    }

    return e(
      'button',
      { onClick: () => this.setState({ liked: true }) },
      'Like'
    );
  }
}
const domContainer = document.querySelector('#app');
ReactDOM.render(e(LikeButton), domContainer);
const domContainer1 = document.querySelector('#app1');
ReactDOM.render(e(LikeButton), domContainer1);